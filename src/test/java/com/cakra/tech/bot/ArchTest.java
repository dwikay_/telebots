package com.cakra.tech.bot;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.cakra.tech.bot");

        noClasses()
            .that()
                .resideInAnyPackage("com.cakra.tech.bot.service..")
            .or()
                .resideInAnyPackage("com.cakra.tech.bot.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..com.cakra.tech.bot.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}

package com.cakra.tech.bot.web.rest;

import com.cakra.tech.bot.CakrasupportBotApp;
import com.cakra.tech.bot.domain.TelegramCommand;
import com.cakra.tech.bot.repository.TelegramCommandRepository;
import com.cakra.tech.bot.service.TelegramCommandService;
import com.cakra.tech.bot.service.dto.TelegramCommandDTO;
import com.cakra.tech.bot.service.mapper.TelegramCommandMapper;
import com.cakra.tech.bot.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.cakra.tech.bot.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link TelegramCommandResource} REST controller.
 */
@SpringBootTest(classes = CakrasupportBotApp.class)
public class TelegramCommandResourceIT {

    private static final String DEFAULT_COMMAND = "AAAAAAAAAA";
    private static final String UPDATED_COMMAND = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private TelegramCommandRepository telegramCommandRepository;

    @Autowired
    private TelegramCommandMapper telegramCommandMapper;

    @Autowired
    private TelegramCommandService telegramCommandService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTelegramCommandMockMvc;

    private TelegramCommand telegramCommand;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TelegramCommandResource telegramCommandResource = new TelegramCommandResource(telegramCommandService);
        this.restTelegramCommandMockMvc = MockMvcBuilders.standaloneSetup(telegramCommandResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TelegramCommand createEntity(EntityManager em) {
        TelegramCommand telegramCommand = new TelegramCommand()
            .command(DEFAULT_COMMAND)
            .description(DEFAULT_DESCRIPTION);
        return telegramCommand;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TelegramCommand createUpdatedEntity(EntityManager em) {
        TelegramCommand telegramCommand = new TelegramCommand()
            .command(UPDATED_COMMAND)
            .description(UPDATED_DESCRIPTION);
        return telegramCommand;
    }

    @BeforeEach
    public void initTest() {
        telegramCommand = createEntity(em);
    }

    @Test
    @Transactional
    public void createTelegramCommand() throws Exception {
        int databaseSizeBeforeCreate = telegramCommandRepository.findAll().size();

        // Create the TelegramCommand
        TelegramCommandDTO telegramCommandDTO = telegramCommandMapper.toDto(telegramCommand);
        restTelegramCommandMockMvc.perform(post("/api/telegram-commands")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(telegramCommandDTO)))
            .andExpect(status().isCreated());

        // Validate the TelegramCommand in the database
        List<TelegramCommand> telegramCommandList = telegramCommandRepository.findAll();
        assertThat(telegramCommandList).hasSize(databaseSizeBeforeCreate + 1);
        TelegramCommand testTelegramCommand = telegramCommandList.get(telegramCommandList.size() - 1);
        assertThat(testTelegramCommand.getCommand()).isEqualTo(DEFAULT_COMMAND);
        assertThat(testTelegramCommand.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createTelegramCommandWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = telegramCommandRepository.findAll().size();

        // Create the TelegramCommand with an existing ID
        telegramCommand.setId(1L);
        TelegramCommandDTO telegramCommandDTO = telegramCommandMapper.toDto(telegramCommand);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTelegramCommandMockMvc.perform(post("/api/telegram-commands")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(telegramCommandDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TelegramCommand in the database
        List<TelegramCommand> telegramCommandList = telegramCommandRepository.findAll();
        assertThat(telegramCommandList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTelegramCommands() throws Exception {
        // Initialize the database
        telegramCommandRepository.saveAndFlush(telegramCommand);

        // Get all the telegramCommandList
        restTelegramCommandMockMvc.perform(get("/api/telegram-commands?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(telegramCommand.getId().intValue())))
            .andExpect(jsonPath("$.[*].command").value(hasItem(DEFAULT_COMMAND)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getTelegramCommand() throws Exception {
        // Initialize the database
        telegramCommandRepository.saveAndFlush(telegramCommand);

        // Get the telegramCommand
        restTelegramCommandMockMvc.perform(get("/api/telegram-commands/{id}", telegramCommand.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(telegramCommand.getId().intValue()))
            .andExpect(jsonPath("$.command").value(DEFAULT_COMMAND))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }

    @Test
    @Transactional
    public void getNonExistingTelegramCommand() throws Exception {
        // Get the telegramCommand
        restTelegramCommandMockMvc.perform(get("/api/telegram-commands/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTelegramCommand() throws Exception {
        // Initialize the database
        telegramCommandRepository.saveAndFlush(telegramCommand);

        int databaseSizeBeforeUpdate = telegramCommandRepository.findAll().size();

        // Update the telegramCommand
        TelegramCommand updatedTelegramCommand = telegramCommandRepository.findById(telegramCommand.getId()).get();
        // Disconnect from session so that the updates on updatedTelegramCommand are not directly saved in db
        em.detach(updatedTelegramCommand);
        updatedTelegramCommand
            .command(UPDATED_COMMAND)
            .description(UPDATED_DESCRIPTION);
        TelegramCommandDTO telegramCommandDTO = telegramCommandMapper.toDto(updatedTelegramCommand);

        restTelegramCommandMockMvc.perform(put("/api/telegram-commands")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(telegramCommandDTO)))
            .andExpect(status().isOk());

        // Validate the TelegramCommand in the database
        List<TelegramCommand> telegramCommandList = telegramCommandRepository.findAll();
        assertThat(telegramCommandList).hasSize(databaseSizeBeforeUpdate);
        TelegramCommand testTelegramCommand = telegramCommandList.get(telegramCommandList.size() - 1);
        assertThat(testTelegramCommand.getCommand()).isEqualTo(UPDATED_COMMAND);
        assertThat(testTelegramCommand.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingTelegramCommand() throws Exception {
        int databaseSizeBeforeUpdate = telegramCommandRepository.findAll().size();

        // Create the TelegramCommand
        TelegramCommandDTO telegramCommandDTO = telegramCommandMapper.toDto(telegramCommand);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTelegramCommandMockMvc.perform(put("/api/telegram-commands")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(telegramCommandDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TelegramCommand in the database
        List<TelegramCommand> telegramCommandList = telegramCommandRepository.findAll();
        assertThat(telegramCommandList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTelegramCommand() throws Exception {
        // Initialize the database
        telegramCommandRepository.saveAndFlush(telegramCommand);

        int databaseSizeBeforeDelete = telegramCommandRepository.findAll().size();

        // Delete the telegramCommand
        restTelegramCommandMockMvc.perform(delete("/api/telegram-commands/{id}", telegramCommand.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TelegramCommand> telegramCommandList = telegramCommandRepository.findAll();
        assertThat(telegramCommandList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

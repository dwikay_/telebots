package com.cakra.tech.bot.web.rest;

import com.cakra.tech.bot.CakrasupportBotApp;
import com.cakra.tech.bot.domain.OmsCommand;
import com.cakra.tech.bot.repository.OmsCommandRepository;
import com.cakra.tech.bot.service.OmsCommandService;
import com.cakra.tech.bot.service.dto.OmsCommandDTO;
import com.cakra.tech.bot.service.mapper.OmsCommandMapper;
import com.cakra.tech.bot.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.cakra.tech.bot.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OmsCommandResource} REST controller.
 */
@SpringBootTest(classes = CakrasupportBotApp.class)
public class OmsCommandResourceIT {

    private static final String DEFAULT_COMMAND = "AAAAAAAAAA";
    private static final String UPDATED_COMMAND = "BBBBBBBBBB";

    private static final Integer DEFAULT_CALL_BACK_QUERY = 1;
    private static final Integer UPDATED_CALL_BACK_QUERY = 2;

    private static final String DEFAULT_TIPE = "AAAAAAAAAA";
    private static final String UPDATED_TIPE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private OmsCommandRepository omsCommandRepository;

    @Autowired
    private OmsCommandMapper omsCommandMapper;

    @Autowired
    private OmsCommandService omsCommandService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOmsCommandMockMvc;

    private OmsCommand omsCommand;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OmsCommandResource omsCommandResource = new OmsCommandResource(omsCommandService);
        this.restOmsCommandMockMvc = MockMvcBuilders.standaloneSetup(omsCommandResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OmsCommand createEntity(EntityManager em) {
        OmsCommand omsCommand = new OmsCommand()
            .command(DEFAULT_COMMAND)
            .callBackQuery(DEFAULT_CALL_BACK_QUERY)
            .tipe(DEFAULT_TIPE)
            .description(DEFAULT_DESCRIPTION);
        return omsCommand;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OmsCommand createUpdatedEntity(EntityManager em) {
        OmsCommand omsCommand = new OmsCommand()
            .command(UPDATED_COMMAND)
            .callBackQuery(UPDATED_CALL_BACK_QUERY)
            .tipe(UPDATED_TIPE)
            .description(UPDATED_DESCRIPTION);
        return omsCommand;
    }

    @BeforeEach
    public void initTest() {
        omsCommand = createEntity(em);
    }

    @Test
    @Transactional
    public void createOmsCommand() throws Exception {
        int databaseSizeBeforeCreate = omsCommandRepository.findAll().size();

        // Create the OmsCommand
        OmsCommandDTO omsCommandDTO = omsCommandMapper.toDto(omsCommand);
        restOmsCommandMockMvc.perform(post("/api/oms-commands")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(omsCommandDTO)))
            .andExpect(status().isCreated());

        // Validate the OmsCommand in the database
        List<OmsCommand> omsCommandList = omsCommandRepository.findAll();
        assertThat(omsCommandList).hasSize(databaseSizeBeforeCreate + 1);
        OmsCommand testOmsCommand = omsCommandList.get(omsCommandList.size() - 1);
        assertThat(testOmsCommand.getCommand()).isEqualTo(DEFAULT_COMMAND);
        assertThat(testOmsCommand.getCallBackQuery()).isEqualTo(DEFAULT_CALL_BACK_QUERY);
        assertThat(testOmsCommand.getTipe()).isEqualTo(DEFAULT_TIPE);
        assertThat(testOmsCommand.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createOmsCommandWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = omsCommandRepository.findAll().size();

        // Create the OmsCommand with an existing ID
        omsCommand.setId(1L);
        OmsCommandDTO omsCommandDTO = omsCommandMapper.toDto(omsCommand);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOmsCommandMockMvc.perform(post("/api/oms-commands")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(omsCommandDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OmsCommand in the database
        List<OmsCommand> omsCommandList = omsCommandRepository.findAll();
        assertThat(omsCommandList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOmsCommands() throws Exception {
        // Initialize the database
        omsCommandRepository.saveAndFlush(omsCommand);

        // Get all the omsCommandList
        restOmsCommandMockMvc.perform(get("/api/oms-commands?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(omsCommand.getId().intValue())))
            .andExpect(jsonPath("$.[*].command").value(hasItem(DEFAULT_COMMAND)))
            .andExpect(jsonPath("$.[*].callBackQuery").value(hasItem(DEFAULT_CALL_BACK_QUERY)))
            .andExpect(jsonPath("$.[*].tipe").value(hasItem(DEFAULT_TIPE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getOmsCommand() throws Exception {
        // Initialize the database
        omsCommandRepository.saveAndFlush(omsCommand);

        // Get the omsCommand
        restOmsCommandMockMvc.perform(get("/api/oms-commands/{id}", omsCommand.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(omsCommand.getId().intValue()))
            .andExpect(jsonPath("$.command").value(DEFAULT_COMMAND))
            .andExpect(jsonPath("$.callBackQuery").value(DEFAULT_CALL_BACK_QUERY))
            .andExpect(jsonPath("$.tipe").value(DEFAULT_TIPE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }

    @Test
    @Transactional
    public void getNonExistingOmsCommand() throws Exception {
        // Get the omsCommand
        restOmsCommandMockMvc.perform(get("/api/oms-commands/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOmsCommand() throws Exception {
        // Initialize the database
        omsCommandRepository.saveAndFlush(omsCommand);

        int databaseSizeBeforeUpdate = omsCommandRepository.findAll().size();

        // Update the omsCommand
        OmsCommand updatedOmsCommand = omsCommandRepository.findById(omsCommand.getId()).get();
        // Disconnect from session so that the updates on updatedOmsCommand are not directly saved in db
        em.detach(updatedOmsCommand);
        updatedOmsCommand
            .command(UPDATED_COMMAND)
            .callBackQuery(UPDATED_CALL_BACK_QUERY)
            .tipe(UPDATED_TIPE)
            .description(UPDATED_DESCRIPTION);
        OmsCommandDTO omsCommandDTO = omsCommandMapper.toDto(updatedOmsCommand);

        restOmsCommandMockMvc.perform(put("/api/oms-commands")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(omsCommandDTO)))
            .andExpect(status().isOk());

        // Validate the OmsCommand in the database
        List<OmsCommand> omsCommandList = omsCommandRepository.findAll();
        assertThat(omsCommandList).hasSize(databaseSizeBeforeUpdate);
        OmsCommand testOmsCommand = omsCommandList.get(omsCommandList.size() - 1);
        assertThat(testOmsCommand.getCommand()).isEqualTo(UPDATED_COMMAND);
        assertThat(testOmsCommand.getCallBackQuery()).isEqualTo(UPDATED_CALL_BACK_QUERY);
        assertThat(testOmsCommand.getTipe()).isEqualTo(UPDATED_TIPE);
        assertThat(testOmsCommand.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingOmsCommand() throws Exception {
        int databaseSizeBeforeUpdate = omsCommandRepository.findAll().size();

        // Create the OmsCommand
        OmsCommandDTO omsCommandDTO = omsCommandMapper.toDto(omsCommand);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOmsCommandMockMvc.perform(put("/api/oms-commands")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(omsCommandDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OmsCommand in the database
        List<OmsCommand> omsCommandList = omsCommandRepository.findAll();
        assertThat(omsCommandList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOmsCommand() throws Exception {
        // Initialize the database
        omsCommandRepository.saveAndFlush(omsCommand);

        int databaseSizeBeforeDelete = omsCommandRepository.findAll().size();

        // Delete the omsCommand
        restOmsCommandMockMvc.perform(delete("/api/oms-commands/{id}", omsCommand.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OmsCommand> omsCommandList = omsCommandRepository.findAll();
        assertThat(omsCommandList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package com.cakra.tech.bot.web.rest;

import com.cakra.tech.bot.CakrasupportBotApp;
import com.cakra.tech.bot.domain.UserTelegram;
import com.cakra.tech.bot.repository.UserTelegramRepository;
import com.cakra.tech.bot.service.UserTelegramService;
import com.cakra.tech.bot.service.dto.UserTelegramDTO;
import com.cakra.tech.bot.service.mapper.UserTelegramMapper;
import com.cakra.tech.bot.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.cakra.tech.bot.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserTelegramResource} REST controller.
 */
@SpringBootTest(classes = CakrasupportBotApp.class)
public class UserTelegramResourceIT {

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_FULLNAME = "AAAAAAAAAA";
    private static final String UPDATED_FULLNAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_BOT = false;
    private static final Boolean UPDATED_IS_BOT = true;

    private static final Long DEFAULT_CHAT_ID = 1L;
    private static final Long UPDATED_CHAT_ID = 2L;

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    @Autowired
    private UserTelegramRepository userTelegramRepository;

    @Autowired
    private UserTelegramMapper userTelegramMapper;

    @Autowired
    private UserTelegramService userTelegramService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserTelegramMockMvc;

    private UserTelegram userTelegram;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserTelegramResource userTelegramResource = new UserTelegramResource(userTelegramService);
        this.restUserTelegramMockMvc = MockMvcBuilders.standaloneSetup(userTelegramResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserTelegram createEntity(EntityManager em) {
        UserTelegram userTelegram = new UserTelegram()
            .username(DEFAULT_USERNAME)
            .fullname(DEFAULT_FULLNAME)
            .isBot(DEFAULT_IS_BOT)
            .chatId(DEFAULT_CHAT_ID)
            .status(DEFAULT_STATUS);
        return userTelegram;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserTelegram createUpdatedEntity(EntityManager em) {
        UserTelegram userTelegram = new UserTelegram()
            .username(UPDATED_USERNAME)
            .fullname(UPDATED_FULLNAME)
            .isBot(UPDATED_IS_BOT)
            .chatId(UPDATED_CHAT_ID)
            .status(UPDATED_STATUS);
        return userTelegram;
    }

    @BeforeEach
    public void initTest() {
        userTelegram = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserTelegram() throws Exception {
        int databaseSizeBeforeCreate = userTelegramRepository.findAll().size();

        // Create the UserTelegram
        UserTelegramDTO userTelegramDTO = userTelegramMapper.toDto(userTelegram);
        restUserTelegramMockMvc.perform(post("/api/user-telegrams")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userTelegramDTO)))
            .andExpect(status().isCreated());

        // Validate the UserTelegram in the database
        List<UserTelegram> userTelegramList = userTelegramRepository.findAll();
        assertThat(userTelegramList).hasSize(databaseSizeBeforeCreate + 1);
        UserTelegram testUserTelegram = userTelegramList.get(userTelegramList.size() - 1);
        assertThat(testUserTelegram.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testUserTelegram.getFullname()).isEqualTo(DEFAULT_FULLNAME);
        assertThat(testUserTelegram.isIsBot()).isEqualTo(DEFAULT_IS_BOT);
        assertThat(testUserTelegram.getChatId()).isEqualTo(DEFAULT_CHAT_ID);
        assertThat(testUserTelegram.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createUserTelegramWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userTelegramRepository.findAll().size();

        // Create the UserTelegram with an existing ID
        userTelegram.setId(1L);
        UserTelegramDTO userTelegramDTO = userTelegramMapper.toDto(userTelegram);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserTelegramMockMvc.perform(post("/api/user-telegrams")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userTelegramDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserTelegram in the database
        List<UserTelegram> userTelegramList = userTelegramRepository.findAll();
        assertThat(userTelegramList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUserTelegrams() throws Exception {
        // Initialize the database
        userTelegramRepository.saveAndFlush(userTelegram);

        // Get all the userTelegramList
        restUserTelegramMockMvc.perform(get("/api/user-telegrams?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userTelegram.getId().intValue())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME)))
            .andExpect(jsonPath("$.[*].fullname").value(hasItem(DEFAULT_FULLNAME)))
            .andExpect(jsonPath("$.[*].isBot").value(hasItem(DEFAULT_IS_BOT.booleanValue())))
            .andExpect(jsonPath("$.[*].chatId").value(hasItem(DEFAULT_CHAT_ID.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }
    
    @Test
    @Transactional
    public void getUserTelegram() throws Exception {
        // Initialize the database
        userTelegramRepository.saveAndFlush(userTelegram);

        // Get the userTelegram
        restUserTelegramMockMvc.perform(get("/api/user-telegrams/{id}", userTelegram.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userTelegram.getId().intValue()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME))
            .andExpect(jsonPath("$.fullname").value(DEFAULT_FULLNAME))
            .andExpect(jsonPath("$.isBot").value(DEFAULT_IS_BOT.booleanValue()))
            .andExpect(jsonPath("$.chatId").value(DEFAULT_CHAT_ID.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    @Transactional
    public void getNonExistingUserTelegram() throws Exception {
        // Get the userTelegram
        restUserTelegramMockMvc.perform(get("/api/user-telegrams/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserTelegram() throws Exception {
        // Initialize the database
        userTelegramRepository.saveAndFlush(userTelegram);

        int databaseSizeBeforeUpdate = userTelegramRepository.findAll().size();

        // Update the userTelegram
        UserTelegram updatedUserTelegram = userTelegramRepository.findById(userTelegram.getId()).get();
        // Disconnect from session so that the updates on updatedUserTelegram are not directly saved in db
        em.detach(updatedUserTelegram);
        updatedUserTelegram
            .username(UPDATED_USERNAME)
            .fullname(UPDATED_FULLNAME)
            .isBot(UPDATED_IS_BOT)
            .chatId(UPDATED_CHAT_ID)
            .status(UPDATED_STATUS);
        UserTelegramDTO userTelegramDTO = userTelegramMapper.toDto(updatedUserTelegram);

        restUserTelegramMockMvc.perform(put("/api/user-telegrams")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userTelegramDTO)))
            .andExpect(status().isOk());

        // Validate the UserTelegram in the database
        List<UserTelegram> userTelegramList = userTelegramRepository.findAll();
        assertThat(userTelegramList).hasSize(databaseSizeBeforeUpdate);
        UserTelegram testUserTelegram = userTelegramList.get(userTelegramList.size() - 1);
        assertThat(testUserTelegram.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testUserTelegram.getFullname()).isEqualTo(UPDATED_FULLNAME);
        assertThat(testUserTelegram.isIsBot()).isEqualTo(UPDATED_IS_BOT);
        assertThat(testUserTelegram.getChatId()).isEqualTo(UPDATED_CHAT_ID);
        assertThat(testUserTelegram.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingUserTelegram() throws Exception {
        int databaseSizeBeforeUpdate = userTelegramRepository.findAll().size();

        // Create the UserTelegram
        UserTelegramDTO userTelegramDTO = userTelegramMapper.toDto(userTelegram);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserTelegramMockMvc.perform(put("/api/user-telegrams")
            .contentType(TestUtil.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(userTelegramDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserTelegram in the database
        List<UserTelegram> userTelegramList = userTelegramRepository.findAll();
        assertThat(userTelegramList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserTelegram() throws Exception {
        // Initialize the database
        userTelegramRepository.saveAndFlush(userTelegram);

        int databaseSizeBeforeDelete = userTelegramRepository.findAll().size();

        // Delete the userTelegram
        restUserTelegramMockMvc.perform(delete("/api/user-telegrams/{id}", userTelegram.getId())
            .accept(TestUtil.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserTelegram> userTelegramList = userTelegramRepository.findAll();
        assertThat(userTelegramList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

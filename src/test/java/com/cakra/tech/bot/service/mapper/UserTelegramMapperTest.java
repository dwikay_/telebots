package com.cakra.tech.bot.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class UserTelegramMapperTest {

    private UserTelegramMapper userTelegramMapper;

    @BeforeEach
    public void setUp() {
        userTelegramMapper = new UserTelegramMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(userTelegramMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(userTelegramMapper.fromId(null)).isNull();
    }
}

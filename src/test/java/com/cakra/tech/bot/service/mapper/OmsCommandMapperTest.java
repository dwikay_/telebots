package com.cakra.tech.bot.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class OmsCommandMapperTest {

    private OmsCommandMapper omsCommandMapper;

    @BeforeEach
    public void setUp() {
        omsCommandMapper = new OmsCommandMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(omsCommandMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(omsCommandMapper.fromId(null)).isNull();
    }
}

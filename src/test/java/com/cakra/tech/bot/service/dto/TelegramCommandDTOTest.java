package com.cakra.tech.bot.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.cakra.tech.bot.web.rest.TestUtil;

public class TelegramCommandDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TelegramCommandDTO.class);
        TelegramCommandDTO telegramCommandDTO1 = new TelegramCommandDTO();
        telegramCommandDTO1.setId(1L);
        TelegramCommandDTO telegramCommandDTO2 = new TelegramCommandDTO();
        assertThat(telegramCommandDTO1).isNotEqualTo(telegramCommandDTO2);
        telegramCommandDTO2.setId(telegramCommandDTO1.getId());
        assertThat(telegramCommandDTO1).isEqualTo(telegramCommandDTO2);
        telegramCommandDTO2.setId(2L);
        assertThat(telegramCommandDTO1).isNotEqualTo(telegramCommandDTO2);
        telegramCommandDTO1.setId(null);
        assertThat(telegramCommandDTO1).isNotEqualTo(telegramCommandDTO2);
    }
}

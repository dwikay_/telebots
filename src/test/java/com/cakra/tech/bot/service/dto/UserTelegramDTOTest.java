package com.cakra.tech.bot.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.cakra.tech.bot.web.rest.TestUtil;

public class UserTelegramDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserTelegramDTO.class);
        UserTelegramDTO userTelegramDTO1 = new UserTelegramDTO();
        userTelegramDTO1.setId(1L);
        UserTelegramDTO userTelegramDTO2 = new UserTelegramDTO();
        assertThat(userTelegramDTO1).isNotEqualTo(userTelegramDTO2);
        userTelegramDTO2.setId(userTelegramDTO1.getId());
        assertThat(userTelegramDTO1).isEqualTo(userTelegramDTO2);
        userTelegramDTO2.setId(2L);
        assertThat(userTelegramDTO1).isNotEqualTo(userTelegramDTO2);
        userTelegramDTO1.setId(null);
        assertThat(userTelegramDTO1).isNotEqualTo(userTelegramDTO2);
    }
}

package com.cakra.tech.bot.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.cakra.tech.bot.web.rest.TestUtil;

public class OmsCommandDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OmsCommandDTO.class);
        OmsCommandDTO omsCommandDTO1 = new OmsCommandDTO();
        omsCommandDTO1.setId(1L);
        OmsCommandDTO omsCommandDTO2 = new OmsCommandDTO();
        assertThat(omsCommandDTO1).isNotEqualTo(omsCommandDTO2);
        omsCommandDTO2.setId(omsCommandDTO1.getId());
        assertThat(omsCommandDTO1).isEqualTo(omsCommandDTO2);
        omsCommandDTO2.setId(2L);
        assertThat(omsCommandDTO1).isNotEqualTo(omsCommandDTO2);
        omsCommandDTO1.setId(null);
        assertThat(omsCommandDTO1).isNotEqualTo(omsCommandDTO2);
    }
}

package com.cakra.tech.bot.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TelegramCommandMapperTest {

    private TelegramCommandMapper telegramCommandMapper;

    @BeforeEach
    public void setUp() {
        telegramCommandMapper = new TelegramCommandMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(telegramCommandMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(telegramCommandMapper.fromId(null)).isNull();
    }
}

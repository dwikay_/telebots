package com.cakra.tech.bot.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.cakra.tech.bot.web.rest.TestUtil;

public class TelegramCommandTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TelegramCommand.class);
        TelegramCommand telegramCommand1 = new TelegramCommand();
        telegramCommand1.setId(1L);
        TelegramCommand telegramCommand2 = new TelegramCommand();
        telegramCommand2.setId(telegramCommand1.getId());
        assertThat(telegramCommand1).isEqualTo(telegramCommand2);
        telegramCommand2.setId(2L);
        assertThat(telegramCommand1).isNotEqualTo(telegramCommand2);
        telegramCommand1.setId(null);
        assertThat(telegramCommand1).isNotEqualTo(telegramCommand2);
    }
}

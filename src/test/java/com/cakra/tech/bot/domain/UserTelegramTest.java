package com.cakra.tech.bot.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.cakra.tech.bot.web.rest.TestUtil;

public class UserTelegramTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserTelegram.class);
        UserTelegram userTelegram1 = new UserTelegram();
        userTelegram1.setId(1L);
        UserTelegram userTelegram2 = new UserTelegram();
        userTelegram2.setId(userTelegram1.getId());
        assertThat(userTelegram1).isEqualTo(userTelegram2);
        userTelegram2.setId(2L);
        assertThat(userTelegram1).isNotEqualTo(userTelegram2);
        userTelegram1.setId(null);
        assertThat(userTelegram1).isNotEqualTo(userTelegram2);
    }
}

package com.cakra.tech.bot.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.cakra.tech.bot.web.rest.TestUtil;

public class OmsCommandTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OmsCommand.class);
        OmsCommand omsCommand1 = new OmsCommand();
        omsCommand1.setId(1L);
        OmsCommand omsCommand2 = new OmsCommand();
        omsCommand2.setId(omsCommand1.getId());
        assertThat(omsCommand1).isEqualTo(omsCommand2);
        omsCommand2.setId(2L);
        assertThat(omsCommand1).isNotEqualTo(omsCommand2);
        omsCommand1.setId(null);
        assertThat(omsCommand1).isNotEqualTo(omsCommand2);
    }
}

/**
 * View Models used by Spring MVC REST controllers.
 */
package com.cakra.tech.bot.web.rest.vm;

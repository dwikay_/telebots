package com.cakra.tech.bot.web.rest;

import com.cakra.tech.bot.service.TelegramCommandService;
import com.cakra.tech.bot.web.rest.errors.BadRequestAlertException;
import com.cakra.tech.bot.service.dto.TelegramCommandDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.cakra.tech.bot.domain.TelegramCommand}.
 */
@RestController
@RequestMapping("/api")
public class TelegramCommandResource {

    private final Logger log = LoggerFactory.getLogger(TelegramCommandResource.class);

    private static final String ENTITY_NAME = "telegramCommand";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TelegramCommandService telegramCommandService;

    public TelegramCommandResource(TelegramCommandService telegramCommandService) {
        this.telegramCommandService = telegramCommandService;
    }

    /**
     * {@code POST  /telegram-commands} : Create a new telegramCommand.
     *
     * @param telegramCommandDTO the telegramCommandDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new telegramCommandDTO, or with status {@code 400 (Bad Request)} if the telegramCommand has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/telegram-commands")
    public ResponseEntity<TelegramCommandDTO> createTelegramCommand(@RequestBody TelegramCommandDTO telegramCommandDTO) throws URISyntaxException {
        log.debug("REST request to save TelegramCommand : {}", telegramCommandDTO);
        if (telegramCommandDTO.getId() != null) {
            throw new BadRequestAlertException("A new telegramCommand cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TelegramCommandDTO result = telegramCommandService.save(telegramCommandDTO);
        return ResponseEntity.created(new URI("/api/telegram-commands/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /telegram-commands} : Updates an existing telegramCommand.
     *
     * @param telegramCommandDTO the telegramCommandDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated telegramCommandDTO,
     * or with status {@code 400 (Bad Request)} if the telegramCommandDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the telegramCommandDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/telegram-commands")
    public ResponseEntity<TelegramCommandDTO> updateTelegramCommand(@RequestBody TelegramCommandDTO telegramCommandDTO) throws URISyntaxException {
        log.debug("REST request to update TelegramCommand : {}", telegramCommandDTO);
        if (telegramCommandDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TelegramCommandDTO result = telegramCommandService.save(telegramCommandDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, telegramCommandDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /telegram-commands} : get all the telegramCommands.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of telegramCommands in body.
     */
    @GetMapping("/telegram-commands")
    public List<TelegramCommandDTO> getAllTelegramCommands() {
        log.debug("REST request to get all TelegramCommands");
        return telegramCommandService.findAll();
    }

    /**
     * {@code GET  /telegram-commands/:id} : get the "id" telegramCommand.
     *
     * @param id the id of the telegramCommandDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the telegramCommandDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/telegram-commands/{id}")
    public ResponseEntity<TelegramCommandDTO> getTelegramCommand(@PathVariable Long id) {
        log.debug("REST request to get TelegramCommand : {}", id);
        Optional<TelegramCommandDTO> telegramCommandDTO = telegramCommandService.findOne(id);
        return ResponseUtil.wrapOrNotFound(telegramCommandDTO);
    }

    /**
     * {@code DELETE  /telegram-commands/:id} : delete the "id" telegramCommand.
     *
     * @param id the id of the telegramCommandDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/telegram-commands/{id}")
    public ResponseEntity<Void> deleteTelegramCommand(@PathVariable Long id) {
        log.debug("REST request to delete TelegramCommand : {}", id);
        telegramCommandService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}

package com.cakra.tech.bot.web.rest;

import com.cakra.tech.bot.service.OmsCommandService;
import com.cakra.tech.bot.web.rest.errors.BadRequestAlertException;
import com.cakra.tech.bot.service.dto.OmsCommandDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.cakra.tech.bot.domain.OmsCommand}.
 */
@RestController
@RequestMapping("/api")
public class OmsCommandResource {

    private final Logger log = LoggerFactory.getLogger(OmsCommandResource.class);

    private static final String ENTITY_NAME = "omsCommand";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OmsCommandService omsCommandService;

    public OmsCommandResource(OmsCommandService omsCommandService) {
        this.omsCommandService = omsCommandService;
    }

    /**
     * {@code POST  /oms-commands} : Create a new omsCommand.
     *
     * @param omsCommandDTO the omsCommandDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new omsCommandDTO, or with status {@code 400 (Bad Request)} if the omsCommand has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/oms-commands")
    public ResponseEntity<OmsCommandDTO> createOmsCommand(@RequestBody OmsCommandDTO omsCommandDTO) throws URISyntaxException {
        log.debug("REST request to save OmsCommand : {}", omsCommandDTO);
        if (omsCommandDTO.getId() != null) {
            throw new BadRequestAlertException("A new omsCommand cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OmsCommandDTO result = omsCommandService.save(omsCommandDTO);
        return ResponseEntity.created(new URI("/api/oms-commands/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /oms-commands} : Updates an existing omsCommand.
     *
     * @param omsCommandDTO the omsCommandDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated omsCommandDTO,
     * or with status {@code 400 (Bad Request)} if the omsCommandDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the omsCommandDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/oms-commands")
    public ResponseEntity<OmsCommandDTO> updateOmsCommand(@RequestBody OmsCommandDTO omsCommandDTO) throws URISyntaxException {
        log.debug("REST request to update OmsCommand : {}", omsCommandDTO);
        if (omsCommandDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OmsCommandDTO result = omsCommandService.save(omsCommandDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, omsCommandDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /oms-commands} : get all the omsCommands.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of omsCommands in body.
     */
    @GetMapping("/oms-commands")
    public List<OmsCommandDTO> getAllOmsCommands() {
        log.debug("REST request to get all OmsCommands");
        return omsCommandService.findAll();
    }

    /**
     * {@code GET  /oms-commands/:id} : get the "id" omsCommand.
     *
     * @param id the id of the omsCommandDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the omsCommandDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/oms-commands/{id}")
    public ResponseEntity<OmsCommandDTO> getOmsCommand(@PathVariable Long id) {
        log.debug("REST request to get OmsCommand : {}", id);
        Optional<OmsCommandDTO> omsCommandDTO = omsCommandService.findOne(id);
        return ResponseUtil.wrapOrNotFound(omsCommandDTO);
    }

    /**
     * {@code DELETE  /oms-commands/:id} : delete the "id" omsCommand.
     *
     * @param id the id of the omsCommandDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/oms-commands/{id}")
    public ResponseEntity<Void> deleteOmsCommand(@PathVariable Long id) {
        log.debug("REST request to delete OmsCommand : {}", id);
        omsCommandService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}

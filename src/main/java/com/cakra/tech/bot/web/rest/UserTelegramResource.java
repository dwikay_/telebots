package com.cakra.tech.bot.web.rest;

import com.cakra.tech.bot.service.UserTelegramService;
import com.cakra.tech.bot.web.rest.errors.BadRequestAlertException;
import com.cakra.tech.bot.service.dto.UserTelegramDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.cakra.tech.bot.domain.UserTelegram}.
 */
@RestController
@RequestMapping("/api")
public class UserTelegramResource {

    private final Logger log = LoggerFactory.getLogger(UserTelegramResource.class);

    private static final String ENTITY_NAME = "userTelegram";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserTelegramService userTelegramService;

    public UserTelegramResource(UserTelegramService userTelegramService) {
        this.userTelegramService = userTelegramService;
    }

    /**
     * {@code POST  /user-telegrams} : Create a new userTelegram.
     *
     * @param userTelegramDTO the userTelegramDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userTelegramDTO, or with status {@code 400 (Bad Request)} if the userTelegram has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-telegrams")
    public ResponseEntity<UserTelegramDTO> createUserTelegram(@RequestBody UserTelegramDTO userTelegramDTO) throws URISyntaxException {
        log.debug("REST request to save UserTelegram : {}", userTelegramDTO);
        if (userTelegramDTO.getId() != null) {
            throw new BadRequestAlertException("A new userTelegram cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserTelegramDTO result = userTelegramService.save(userTelegramDTO);
        return ResponseEntity.created(new URI("/api/user-telegrams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-telegrams} : Updates an existing userTelegram.
     *
     * @param userTelegramDTO the userTelegramDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userTelegramDTO,
     * or with status {@code 400 (Bad Request)} if the userTelegramDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userTelegramDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-telegrams")
    public ResponseEntity<UserTelegramDTO> updateUserTelegram(@RequestBody UserTelegramDTO userTelegramDTO) throws URISyntaxException {
        log.debug("REST request to update UserTelegram : {}", userTelegramDTO);
        if (userTelegramDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserTelegramDTO result = userTelegramService.save(userTelegramDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, userTelegramDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-telegrams} : get all the userTelegrams.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userTelegrams in body.
     */
    @GetMapping("/user-telegrams")
    public ResponseEntity<List<UserTelegramDTO>> getAllUserTelegrams(Pageable pageable) {
        log.debug("REST request to get a page of UserTelegrams");
        Page<UserTelegramDTO> page = userTelegramService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /user-telegrams/:id} : get the "id" userTelegram.
     *
     * @param id the id of the userTelegramDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userTelegramDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-telegrams/{id}")
    public ResponseEntity<UserTelegramDTO> getUserTelegram(@PathVariable Long id) {
        log.debug("REST request to get UserTelegram : {}", id);
        Optional<UserTelegramDTO> userTelegramDTO = userTelegramService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userTelegramDTO);
    }

    /**
     * {@code DELETE  /user-telegrams/:id} : delete the "id" userTelegram.
     *
     * @param id the id of the userTelegramDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-telegrams/{id}")
    public ResponseEntity<Void> deleteUserTelegram(@PathVariable Long id) {
        log.debug("REST request to delete UserTelegram : {}", id);
        userTelegramService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}

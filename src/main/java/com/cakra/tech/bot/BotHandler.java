package com.cakra.tech.bot;

import com.cakra.tech.bot.config.bot.BotConfig;
import com.cakra.tech.bot.service.ClientService;
import com.cakra.tech.bot.service.OmsCommandService;
import com.cakra.tech.bot.service.TelegramCommandService;
import com.cakra.tech.bot.service.UserTelegramService;
import com.cakra.tech.bot.service.dto.ClientDTO;
import com.cakra.tech.bot.service.dto.OmsCommandDTO;
import com.cakra.tech.bot.service.dto.TelegramCommandDTO;
import com.cakra.tech.bot.service.dto.UserTelegramDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.cakra.tech.bot.utils.StaticStateVariables.*;


@Service
//@Component
@Transactional
public class BotHandler extends TelegramLongPollingBot {
    private final Logger log = LoggerFactory.getLogger(BotHandler.class);
    @Autowired
    UserTelegramService userTelegramService;
    @Autowired
    ClientService clientService;
    @Autowired
    OmsCommandService omsCommandService;

    @Autowired
    TelegramCommandService telegramCommandService;

    @Override
    public String getBotUsername() {
        return BotConfig.BOT_USERNAME;
    }
    @Override
    public String getBotToken() {
        return BotConfig.BOT_TOKEN;
    }
    @Override
    public void onUpdateReceived(Update update) {
        log.info(">>>> onUpdateReceived : PASS");
        try {

            if (update.hasMessage()) {
                Message message = update.getMessage();
                Optional<UserTelegramDTO> reqInfo = null;
                reqInfo = userTelegramService.getOneByChatId(message.getChatId());
                incomingMessageHandler(message, reqInfo);
            }
            /*
                perbandingan data : callback query
            */
            else if (update.hasCallbackQuery()) {

                log.info(">>>> REQUEST IF update.hasCallbackQuery()");
                CallbackQuery callbackQuery = update.getCallbackQuery();
                Optional<UserTelegramDTO> reqInfoo = userTelegramService.getOneByChatId(callbackQuery.getFrom().getId());
                log.info(">>>>>>>>>>> INCOMING CALLBACKQUERY DATA : " + callbackQuery.getData());
                log.info(">>>> REQUEST SWITCH CASE");
                switch (callbackQuery.getData()){
                    case CHECK_HEALTH:
                        log.info(">>>> CASE: CHECK_HEALTH : PROCESS)");
                        sendCheckHealthClientCommand(reqInfoo);
                        log.info(">>>> REQUEST sendCheckHealthKeyboardMarkup");
                        break;
                    case "11":
                        log.info(">>>> CASE: CLIENT_LIST_SINARMAS : PROCESS)");
                        sendOmsTypeKeyboadMarkup(callbackQuery, reqInfoo);
                        log.info(">>>> REQUEST sendOmsTypeKeyboadMarkup");
                        break;
                    case "12":
                        log.info(">>>> CASE: CLIENT_LIST_STOCKBIT : PROCESS)");
                        sendOmsTypeKeyboadMarkup(callbackQuery, reqInfoo);
                        log.info(">>>> REQUEST sendOmsTypeKeyboadMarkup");
                        break;
                    case "13":
                        log.info(">>>> CASE: CLIENT_LIST_TRIMEGAH : PROCESS)");
                        sendOmsTypeKeyboadMarkup(callbackQuery, reqInfoo);
                        log.info(">>>> REQUEST sendOmsTypeKeyboadMarkup");
                        break;
                    case OMS_TYPE_INSTI:
                        log.info(">>>> CASE: OMS_TYPE_INSTI : PROCESS)");
                        sendInstiProcess(callbackQuery, reqInfoo);
                        log.info(">>>> REQUEST sendInstiProcess");
                        break;
                    case OMS_TYPE_RETAIL:
                        log.info(">>>> CASE: OMS_TYPE_RETAIL : PROCESS)");
                        sendRetailProcess(callbackQuery, reqInfoo);
                        log.info(">>>> REQUEST sendRetailProcess");
                        break;
                    case OMS_TYPE:
                        log.info(">>>> CASE: OMS_TYPE : PROCESS)");
                        sendOmsTypeKeyboadMarkup(callbackQuery, reqInfoo);
                        log.info(">>>> REQUEST sendOmsTypeKeyboadMarkup");
                        break;
                    default:
                        break;
                }

            }
        }catch (Exception e){

            e.printStackTrace();
        }

    }

    private void incomingMessageHandler(Message message, Optional<UserTelegramDTO> reqInfo) throws TelegramApiException {
        log.info(">>>> getOneByChatId : PASS");
        UserTelegramDTO userTelegramDTO = new UserTelegramDTO();
        SendMessage sendMessageRequest = new SendMessage();

        if(reqInfo.isPresent()) {
            log.info(">>>> isPresent : PASS");
            if (isCommandForStart(message.getText())) {
                log.info(">>>> isCommandForOther : PASS");

                sendStartingKeyboardMarkup(message);

            } else if (!isCommandForStart(message.getText()) || !isCommandForHealthCheck(message.getText())){
                log.info(">>>> !isCommandForOther : PASS");

                sendMessageRequest.setChatId(reqInfo.get().getChatId()); // get via db
                sendMessageRequest.setText("Perintah " + message.getText() + " Tidak Di temukan\n");
                execute(sendMessageRequest);
                sendDefaultKeyboadMarkup(message);
                //defaultMessage(message);
                log.info(">>>> EXECUTED : sendMessageRequest : !isCommandForOther : {}",sendMessageRequest.getText());
            }
        } else {
            log.info(">>>> REQUEST UPDATE TO UserTelegramDTO");
            userTelegramDTO.setChatId(message.getFrom().getId());
            userTelegramDTO.setFullname(message.getFrom().getFirstName());
            userTelegramDTO.setIsBot(message.getFrom().getIsBot());
            userTelegramDTO.setUsername(message.getFrom().getUserName());
            userTelegramDTO.setStatus(INITIAL_STATE);
            log.info(">>>> Inserting {} with chatId {} TO DB.....", message.getFrom().getFirstName(), message.getFrom().getId());
            log.info(">>>> Inserting Ke DB : PASS");
            userTelegramService.save(userTelegramDTO);
            log.info(">>>> Saving : PASS");

            defaultStartMessage(message);
            //sendDefaultKeyboadMarkup(message);
            sendStartingKeyboardMarkup(message);
            log.info(">>>> Request sendStartingKeyboardMarkup");
        }return;
    }
    private static boolean isCommandForStart(String text) {
        boolean isSimpleCommand = text.equals("/start");
        return text.startsWith("/") && isSimpleCommand;
    }
    private static boolean isCommandForHealthCheck(String text){
        boolean command = text.equals("/healthcheck");
        return text.startsWith("/") && command;
    }
    private void sendOmsTypeKeyboadMarkup(CallbackQuery callbackQuery, Optional<UserTelegramDTO> reqInfoo){

        log.info(">>>> sendOmsTypeKeyboadMarkup : PROCESS");
        reqInfoo.get().setStatus(Integer.valueOf(callbackQuery.getData()));
        userTelegramService.save(reqInfoo.get());
        Optional<ClientDTO> ClientId = clientService.findOne(Long.valueOf(callbackQuery.getData()));
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(reqInfoo.get().getChatId());
        sendMessage.setText("Silahkan Pilih Tipe OMS dari Client " + ClientId.get().getCompanyName() + " : \n");

        //Optional<ClientDTO> clientType = clientService.getOneByClientType(Integer.valueOf(callbackQuery.getData()));
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineButtons = new ArrayList<>();

        List<InlineKeyboardButton> rowKe1 = new ArrayList<>();//row1
        List<InlineKeyboardButton> rowKe2 = new ArrayList<>();//row2

        InlineKeyboardButton row1Button1 = new InlineKeyboardButton();
        InlineKeyboardButton row1Button2 = new InlineKeyboardButton();
        InlineKeyboardButton row2BackButton = new InlineKeyboardButton();

        log.info(">>>> SWITCH ADDING BUTTONS BY OMSTYPE : PROCESS");
        switch (Integer.valueOf(ClientId.get().getOmsType())){

            case 1 :
                log.info(">>>> CASE 1 : PROCESS");
                row1Button1.setText(INSTI);
                row1Button1.setCallbackData(OMS_TYPE_INSTI);

//                reqInfoo.get().setStatus(OMS_TYPE_INSTI_INT);

                row2BackButton.setText("Kembali");
                row2BackButton.setCallbackData(CHECK_HEALTH);
//                reqInfoo.get().setStatus(Integer.valueOf(CHECK_HEALTH));


                rowKe1.add(row1Button1);
                rowKe2.add(row2BackButton);
                log.info(">>>> CASE 1 : DONE");
                break;
            case 2:
                log.info(">>>> CASE 2 : PROCESS");
                row1Button1.setText(RETAIL);
                row1Button1.setCallbackData(OMS_TYPE_RETAIL);
//                reqInfoo.get().setStatus(OMS_TYPE_RETAIL_INT);

                row2BackButton.setText("Kembali");
                row2BackButton.setCallbackData(CHECK_HEALTH);
//                reqInfoo.get().setStatus(Integer.valueOf(CHECK_HEALTH));

                rowKe1.add(row1Button1);
                rowKe2.add(row2BackButton);
                log.info(">>>> CASE 2 : DONE");
                break;
            case 3:
                log.info(">>>> CASE 3 : PROCESS");
                row1Button1.setText(INSTI);
                row1Button1.setCallbackData(OMS_TYPE_INSTI);
//                reqInfoo.get().setStatus(OMS_TYPE_INSTI_INT);

                row1Button2.setText(RETAIL);
                row1Button2.setCallbackData(OMS_TYPE_RETAIL);
//                reqInfoo.get().setStatus(OMS_TYPE_RETAIL_INT);

                row2BackButton.setText("Kembali");
                row2BackButton.setCallbackData(CHECK_HEALTH);
//                reqInfoo.get().setStatus(Integer.valueOf(CHECK_HEALTH));

                rowKe1.add(row1Button1);
                rowKe1.add(row1Button2);
                rowKe2.add(row2BackButton);
                log.info(">>>> CASE 3 : DONE");
                break;
            default:
                log.info(">>>> coba cek lagi di flow sendOmsTypeKeyboadMarkup");
                break;
        }

        inlineButtons.add(rowKe1);
        inlineButtons.add(rowKe2);
        inlineKeyboardMarkup.setKeyboard(inlineButtons);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        try{
            execute(sendMessage);
            log.info(">>>> EXECUTED sendOmsTypeKeyboadMarkup");
        } catch (TelegramApiException e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
    private void defaultStartMessage(Message message) {
        log.info(">>>> messageOnMainMenu : PROCESS");
        Optional<UserTelegramDTO> req = userTelegramService.getOneByChatId(message.getChatId());
        SendMessage sendMessageRequest = new SendMessage();
        sendMessageRequest.setChatId(req.get().getChatId());
        sendMessageRequest.setText(" Selamat Datang di Bot Telegram Support Cakra\n"
        + "Perintah yang tersedia : \n");
        try {
            execute(sendMessageRequest);
            log.info(">>>> messageOnMainMenu.sendMessageRequest : OK_SEND");
            log.info(" >>>> EXECUTED : messageOnMainMenu.sendMessageRequest");
        } catch (TelegramApiException e) {
            e.printStackTrace();
           log.info("gagal mengirim pesan");
        }
        log.info(">>>> messageOnMainMenu : PASS");
    }
    private void sendStartingKeyboardMarkup(Message message){
        Optional<UserTelegramDTO> reqInfo = userTelegramService.getOneByChatId(message.getChatId());
        log.info(">>>> sendStartingKeyboardMarkup  : PROCESS");

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(reqInfo.get().getChatId());
        sendMessage.setText("Silahkan Pilih :\n");

//        reqInfo.get().setStatus(START_STATE);
        userTelegramService.save(reqInfo.get());
        log.info(">>>> SET STATUS TO START_STATE 1 : PASS");
        //inisialisasi keyboard
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineButtons = new ArrayList<>();
        List<TelegramCommandDTO> commandDTOS = telegramCommandService.findAll();
        List<InlineKeyboardButton> inlineKeyboardButtonList = new ArrayList<>();

        for(TelegramCommandDTO commandDTO : commandDTOS){

            InlineKeyboardButton commandButton1 = new InlineKeyboardButton();
            commandButton1.setText(commandDTO.getCommand());
            commandButton1.setCallbackData(CHECK_HEALTH);
//            reqInfoo.get().setStatus(Integer.valueOf(rowButton1.getCallbackData()));

            inlineKeyboardButtonList.add(commandButton1);
        }
        inlineButtons.add(inlineKeyboardButtonList);//inlineKeyboardButtonList ke inlineButtons
        inlineKeyboardMarkup.setKeyboard(inlineButtons);//markup inlineButtons
        // Add it to the message
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        try{

            execute(sendMessage);
            log.info(">>>> EXECUTED sendMessage to {}",reqInfo.get().getFullname());
        } catch (TelegramApiException e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
    private void sendCheckHealthClientCommand(Optional<UserTelegramDTO> reqInfoo){
        log.info(">>>> sendCheckHealthKeyboardMarkup : PROCESS");
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(reqInfoo.get().getChatId());
        sendMessage.setText("Silahkan Pilih Client: \n" + "List Client yang tersedia : \n");


        //inisialisasi keyboard
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineButtons = new ArrayList<>();
        List<ClientDTO> clientDTOList = clientService.getAll();

        List<InlineKeyboardButton> inlineKeyboardButtonList = new ArrayList<>();

        for(ClientDTO clientDTO : clientDTOList){

            InlineKeyboardButton rowButton1 = new InlineKeyboardButton();
            rowButton1.setText(clientDTO.getCompanyName());
            rowButton1.setCallbackData(String.valueOf(clientDTO.getId()));


            inlineKeyboardButtonList.add(rowButton1);
        }
        inlineButtons.add(inlineKeyboardButtonList);//inlineKeyboardButtonList ke inlineButtons
        inlineKeyboardMarkup.setKeyboard(inlineButtons);//markup inlineButtons
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        reqInfoo.get().setStatus(INITIAL_STATE);
        userTelegramService.save(reqInfoo.get());
        try{
            execute(sendMessage);
            log.info(">>>> EXECUTED sendCheckHealthKeyboardMarkup");
        } catch (TelegramApiException e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
    private void sendDefaultKeyboadMarkup(Message message){

        log.info(">>>> sendStartingKeyboardMarkup  : PROCESS");
        Optional<UserTelegramDTO> reqInfo = userTelegramService.getOneByChatId(message.getChatId());
        SendMessage sendMessage = new SendMessage();
        System.out.println(reqInfo.get().getChatId());
        sendMessage.setChatId(reqInfo.get().getChatId());
        sendMessage.setText("Silahkan Pilih :\n");


        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineButtons = new ArrayList<>();

        List<InlineKeyboardButton> inlineKeyboardButtonList = new ArrayList<>();
        InlineKeyboardButton rowButton1 = new InlineKeyboardButton();


        rowButton1.setText("healthcheck");

        rowButton1.setCallbackData(CHECK_HEALTH);
//        reqInfo.get().setStatus(Integer.valueOf(rowButton1.getCallbackData()));
//        userTelegramService.save(reqInfo.get());


        inlineKeyboardButtonList.add(rowButton1);
        inlineButtons.add(inlineKeyboardButtonList);
        inlineKeyboardMarkup.setKeyboard(inlineButtons);
        sendMessage.setChatId(reqInfo.get().getChatId());
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        try{
            execute(sendMessage);
        } catch (TelegramApiException e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    private void sendInstiProcess(CallbackQuery callbackQuery, Optional<UserTelegramDTO> reqInfoo){

        log.info(">>>> sendInstiProcess : PROCESS");
        List<OmsCommandDTO> Insti = omsCommandService.getAllByTipe(callbackQuery.getData());
        Optional<ClientDTO> client = clientService.findOne(Long.valueOf(reqInfoo.get().getStatus()));
        System.out.println(">>>>>>"+ Insti);

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(reqInfoo.get().getChatId());
        sendMessage.setText("Perintah Insti dari Client " + client.get().getCompanyName() + " Yang Tersedia  : \n");

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineButtons = new ArrayList<>();

        List<InlineKeyboardButton> inlineKeyboardButtonList = new ArrayList<>();
        InlineKeyboardButton backbutton = new InlineKeyboardButton();

        backbutton.setText("Kembali");
        backbutton.setCallbackData(CHECK_HEALTH);

        Insti.forEach((data) -> {
            InlineKeyboardButton commandButton = new InlineKeyboardButton();
            List<InlineKeyboardButton> inlineKeyboardButtonList1 = new ArrayList<>();
            log.info(data.getCommand());
            commandButton.setText(data.getCommand());
            commandButton.setCallbackData(String.valueOf(data.getCallBackQuery()));
            inlineKeyboardButtonList1.add(commandButton);
            inlineButtons.add(inlineKeyboardButtonList1);
        });

        inlineKeyboardButtonList.add(backbutton);
        inlineButtons.add(inlineKeyboardButtonList);
        inlineKeyboardMarkup.setKeyboard(inlineButtons);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        try{
            execute(sendMessage);
            log.info(">>>> EXECUTED sendInstiProcess");
        } catch (TelegramApiException e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
    private void sendRetailProcess(CallbackQuery callbackQuery, Optional<UserTelegramDTO> reqInfoo){

        log.info(">>>> sendRetailProcess : PROCESS");


        List<OmsCommandDTO> Retail = omsCommandService.getAllByTipe(callbackQuery.getData());
        Optional<ClientDTO> client = clientService.findOne(Long.valueOf(reqInfoo.get().getStatus()));

        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(reqInfoo.get().getChatId());
        sendMessage.setText("Perintah Retail Client " + client.get().getCompanyName() +  "Yang Tersedia  : \n");

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineButtons = new ArrayList<>();

        List<InlineKeyboardButton> inlineKeyboardButtonList = new ArrayList<>();
        InlineKeyboardButton backbutton = new InlineKeyboardButton();

        backbutton.setText("Kembali");
        backbutton.setCallbackData(CHECK_HEALTH);

        Retail.forEach((data) -> {
            InlineKeyboardButton commandButton = new InlineKeyboardButton();
            List<InlineKeyboardButton> inlineKeyboardButtonList1 = new ArrayList<>();
            log.info(data.getCommand());
            commandButton.setText(data.getCommand());
            commandButton.setCallbackData(String.valueOf(data.getCallBackQuery()));
            inlineKeyboardButtonList1.add(commandButton);
            inlineButtons.add(inlineKeyboardButtonList1);
        });

        inlineKeyboardButtonList.add(backbutton);
        inlineButtons.add(inlineKeyboardButtonList);
        inlineKeyboardMarkup.setKeyboard(inlineButtons);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        try{
            execute(sendMessage);
            log.info(">>>> EXECUTED sendRetailProcess");
        } catch (TelegramApiException e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
}

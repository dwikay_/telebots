package com.cakra.tech.bot.domain.projection;

public interface ICommand {
    String getCommand();
    String getDescription();
}

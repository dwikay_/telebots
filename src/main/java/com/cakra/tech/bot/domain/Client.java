package com.cakra.tech.bot.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A Client.
 */
@Entity
@Table(name = "client")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "host")
    private String host;

    @Column(name = "port")
    private Integer port;

    @Column(name = "path_url")
    private String pathUrl;

    @Column(name = "method")
    private String method;

    @Column(name = "header")
    private String header;

    @Column(name = "client_type")
    private Integer clientType;

    @Column(name = "oms_type")
    private String omsType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public Client companyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getHost() {
        return host;
    }

    public Client host(String host) {
        this.host = host;
        return this;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public Client port(Integer port) {
        this.port = port;
        return this;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getPathUrl() {
        return pathUrl;
    }

    public Client pathUrl(String pathUrl) {
        this.pathUrl = pathUrl;
        return this;
    }

    public void setPathUrl(String pathUrl) {
        this.pathUrl = pathUrl;
    }

    public String getMethod() {
        return method;
    }

    public Client method(String method) {
        this.method = method;
        return this;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getHeader() {
        return header;
    }

    public Client header(String header) {
        this.header = header;
        return this;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Integer getClientType() {
        return clientType;
    }

    public Client clientType(Integer clientType) {
        this.clientType = clientType;
        return this;
    }

    public void setClientType(Integer clientType) {
        this.clientType = clientType;
    }

    public String getOmsType() {
        return omsType;
    }

    public Client omsType(String omsType) {
        this.omsType = omsType;
        return this;
    }

    public void setOmsType(String omsType) {
        this.omsType = omsType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Client)) {
            return false;
        }
        return id != null && id.equals(((Client) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Client{" +
            "id=" + getId() +
            ", companyName='" + getCompanyName() + "'" +
            ", host='" + getHost() + "'" +
            ", port=" + getPort() +
            ", pathUrl='" + getPathUrl() + "'" +
            ", method='" + getMethod() + "'" +
            ", header='" + getHeader() + "'" +
            ", clientType=" + getClientType() +
            ", omsType='" + getOmsType() + "'" +
            "}";
    }
}

package com.cakra.tech.bot.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A OmsCommand.
 */
@Entity
@Table(name = "oms_command")
public class OmsCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "command")
    private String command;

    @Column(name = "call_back_query")
    private Integer callBackQuery;

    @Column(name = "tipe")
    private String tipe;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties("omsCommands")
    private Client client;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCommand() {
        return command;
    }

    public OmsCommand command(String command) {
        this.command = command;
        return this;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Integer getCallBackQuery() {
        return callBackQuery;
    }

    public OmsCommand callBackQuery(Integer callBackQuery) {
        this.callBackQuery = callBackQuery;
        return this;
    }

    public void setCallBackQuery(Integer callBackQuery) {
        this.callBackQuery = callBackQuery;
    }

    public String getTipe() {
        return tipe;
    }

    public OmsCommand tipe(String tipe) {
        this.tipe = tipe;
        return this;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getDescription() {
        return description;
    }

    public OmsCommand description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Client getClient() {
        return client;
    }

    public OmsCommand client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OmsCommand)) {
            return false;
        }
        return id != null && id.equals(((OmsCommand) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OmsCommand{" +
            "id=" + getId() +
            ", command='" + getCommand() + "'" +
            ", callBackQuery=" + getCallBackQuery() +
            ", tipe='" + getTipe() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}

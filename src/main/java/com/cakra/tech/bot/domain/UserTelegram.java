package com.cakra.tech.bot.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A UserTelegram.
 */
@Entity
@Table(name = "user_telegram")
public class UserTelegram implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "fullname")
    private String fullname;

    @Column(name = "is_bot")
    private Boolean isBot;

    @Column(name = "chat_id")
    private Long chatId;

    @Column(name = "status")
    private Integer status;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public UserTelegram username(String username) {
        this.username = username;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public UserTelegram fullname(String fullname) {
        this.fullname = fullname;
        return this;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Boolean isIsBot() {
        return isBot;
    }

    public UserTelegram isBot(Boolean isBot) {
        this.isBot = isBot;
        return this;
    }

    public void setIsBot(Boolean isBot) {
        this.isBot = isBot;
    }

    public Long getChatId() {
        return chatId;
    }

    public UserTelegram chatId(Long chatId) {
        this.chatId = chatId;
        return this;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public Integer getStatus() {
        return status;
    }

    public UserTelegram status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserTelegram)) {
            return false;
        }
        return id != null && id.equals(((UserTelegram) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserTelegram{" +
            "id=" + getId() +
            ", username='" + getUsername() + "'" +
            ", fullname='" + getFullname() + "'" +
            ", isBot='" + isIsBot() + "'" +
            ", chatId=" + getChatId() +
            ", status=" + getStatus() +
            "}";
    }
}

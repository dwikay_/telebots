package com.cakra.tech.bot.repository;

import com.cakra.tech.bot.domain.OmsCommand;

import com.cakra.tech.bot.domain.projection.ICommand;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the OmsCommand entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OmsCommandRepository extends JpaRepository<OmsCommand, Long> {

    List<OmsCommand> findAllByTipe(String tipe);

    @Query(value = "select command,description from oms_command where client_id = :clientId and tipe = :tipe", nativeQuery = true)
    List<ICommand> getCommandByClietId(Long clientId, String tipe);

}

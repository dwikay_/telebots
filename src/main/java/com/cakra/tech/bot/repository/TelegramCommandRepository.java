package com.cakra.tech.bot.repository;

import com.cakra.tech.bot.domain.TelegramCommand;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the TelegramCommand entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TelegramCommandRepository extends JpaRepository<TelegramCommand, Long> {

    Optional<TelegramCommand> findOneByCommand(String command);
}

package com.cakra.tech.bot.repository;

import com.cakra.tech.bot.domain.UserTelegram;

import com.cakra.tech.bot.service.dto.UserTelegramDTO;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the UserTelegram entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserTelegramRepository extends JpaRepository<UserTelegram, Long> {


    Optional<UserTelegram> findByChatId(Long chatId);
    Optional<UserTelegram> findByStatus(Integer status);
}

package com.cakra.tech.bot.repository;

import com.cakra.tech.bot.domain.Client;

import com.cakra.tech.bot.domain.UserTelegram;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Client entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    Optional<Client> findByClientType(Integer clientType);

}

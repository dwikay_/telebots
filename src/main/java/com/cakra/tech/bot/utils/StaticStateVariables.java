package com.cakra.tech.bot.utils;

public class StaticStateVariables {

    public static final int INITIAL_STATE = 0;
    public static final int START_STATE = 1;
    public static final String CHECK_HEALTH = "CHECK_HEALTH";
    public static final String CLIENT_LIST_SINARMAS = "21";
    public static final String CLIENT_LIST_STOCKBIT = "22";
    public static final String CLIENT_LIST_TRIMEGAH = "23";
    public static final String OMS_TYPE = "100";
    public static final String OMS_TYPE_INSTI = "Insti";
    public static final Integer OMS_TYPE_INSTI_INT = 101;
    public static final String OMS_TYPE_RETAIL = "Retail";
    public static final Integer OMS_TYPE_RETAIL_INT = 102;
    public static final String INSTI = "INSTI";
    public static final String RETAIL = "RETAIL";

}

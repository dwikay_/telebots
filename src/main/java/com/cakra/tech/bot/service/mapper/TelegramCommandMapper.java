package com.cakra.tech.bot.service.mapper;


import com.cakra.tech.bot.domain.*;
import com.cakra.tech.bot.service.dto.TelegramCommandDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TelegramCommand} and its DTO {@link TelegramCommandDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TelegramCommandMapper extends EntityMapper<TelegramCommandDTO, TelegramCommand> {



    default TelegramCommand fromId(Long id) {
        if (id == null) {
            return null;
        }
        TelegramCommand telegramCommand = new TelegramCommand();
        telegramCommand.setId(id);
        return telegramCommand;
    }
}

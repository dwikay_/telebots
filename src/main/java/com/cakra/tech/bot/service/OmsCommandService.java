package com.cakra.tech.bot.service;

import com.cakra.tech.bot.service.dto.OmsCommandDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.cakra.tech.bot.domain.OmsCommand}.
 */
public interface OmsCommandService {

    /**
     * Save a omsCommand.
     *
     * @param omsCommandDTO the entity to save.
     * @return the persisted entity.
     */
    OmsCommandDTO save(OmsCommandDTO omsCommandDTO);

    /**
     * Get all the omsCommands.
     *
     * @return the list of entities.
     */
    List<OmsCommandDTO> findAll();

    /**
     * Get the "id" omsCommand.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OmsCommandDTO> findOne(Long id);

    /**
     * Delete the "id" omsCommand.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<OmsCommandDTO> getAllByTipe(String tipe);
}

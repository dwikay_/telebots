package com.cakra.tech.bot.service;

import com.cakra.tech.bot.domain.Client;
import com.cakra.tech.bot.service.dto.UserTelegramDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.cakra.tech.bot.domain.UserTelegram}.
 */
public interface UserTelegramService {

    /**
     * Save a userTelegram.
     *
     * @param userTelegramDTO the entity to save.
     * @return the persisted entity.
     */
    UserTelegramDTO save(UserTelegramDTO userTelegramDTO);

    /**
     * Get all the userTelegrams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserTelegramDTO> findAll(Pageable pageable);

    /**
     * Get the "id" userTelegram.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserTelegramDTO> findOne(Long id);

    /**
     * Delete the "id" userTelegram.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);


    Optional<UserTelegramDTO> getOneByChatId(Long chatId);
    Optional<UserTelegramDTO> getOneByStatus(Integer status);
}

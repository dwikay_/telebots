package com.cakra.tech.bot.service.mapper;


import com.cakra.tech.bot.domain.*;
import com.cakra.tech.bot.service.dto.UserTelegramDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserTelegram} and its DTO {@link UserTelegramDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserTelegramMapper extends EntityMapper<UserTelegramDTO, UserTelegram> {



    default UserTelegram fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserTelegram userTelegram = new UserTelegram();
        userTelegram.setId(id);
        return userTelegram;
    }
}

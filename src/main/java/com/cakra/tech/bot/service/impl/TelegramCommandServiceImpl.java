package com.cakra.tech.bot.service.impl;

import com.cakra.tech.bot.service.TelegramCommandService;
import com.cakra.tech.bot.domain.TelegramCommand;
import com.cakra.tech.bot.repository.TelegramCommandRepository;
import com.cakra.tech.bot.service.dto.TelegramCommandDTO;
import com.cakra.tech.bot.service.mapper.TelegramCommandMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link TelegramCommand}.
 */
@Service
@Transactional
public class TelegramCommandServiceImpl implements TelegramCommandService {

    private final Logger log = LoggerFactory.getLogger(TelegramCommandServiceImpl.class);

    private final TelegramCommandRepository telegramCommandRepository;

    private final TelegramCommandMapper telegramCommandMapper;

    public TelegramCommandServiceImpl(TelegramCommandRepository telegramCommandRepository, TelegramCommandMapper telegramCommandMapper) {
        this.telegramCommandRepository = telegramCommandRepository;
        this.telegramCommandMapper = telegramCommandMapper;
    }

    /**
     * Save a telegramCommand.
     *
     * @param telegramCommandDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TelegramCommandDTO save(TelegramCommandDTO telegramCommandDTO) {
        log.debug("Request to save TelegramCommand : {}", telegramCommandDTO);
        TelegramCommand telegramCommand = telegramCommandMapper.toEntity(telegramCommandDTO);
        telegramCommand = telegramCommandRepository.save(telegramCommand);
        return telegramCommandMapper.toDto(telegramCommand);
    }

    /**
     * Get all the telegramCommands.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<TelegramCommandDTO> findAll() {
        log.debug("Request to get all TelegramCommands");
        return telegramCommandRepository.findAll().stream()
            .map(telegramCommandMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one telegramCommand by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TelegramCommandDTO> findOne(Long id) {
        log.debug("Request to get TelegramCommand : {}", id);
        return telegramCommandRepository.findById(id)
            .map(telegramCommandMapper::toDto);
    }

    /**
     * Delete the telegramCommand by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TelegramCommand : {}", id);
        telegramCommandRepository.deleteById(id);
    }

    @Override
    public Optional<TelegramCommandDTO> getOneByCommand(String command) {
        return  telegramCommandRepository.findOneByCommand(command)
            .map(telegramCommandMapper::toDto);
    }
}

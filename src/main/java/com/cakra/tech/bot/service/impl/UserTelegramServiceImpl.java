package com.cakra.tech.bot.service.impl;

import com.cakra.tech.bot.service.UserTelegramService;
import com.cakra.tech.bot.domain.UserTelegram;
import com.cakra.tech.bot.repository.UserTelegramRepository;
import com.cakra.tech.bot.service.dto.UserTelegramDTO;
import com.cakra.tech.bot.service.mapper.UserTelegramMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link UserTelegram}.
 */
@Service
@Transactional
public class UserTelegramServiceImpl implements UserTelegramService {

    private final Logger log = LoggerFactory.getLogger(UserTelegramServiceImpl.class);

    private final UserTelegramRepository userTelegramRepository;

    private final UserTelegramMapper userTelegramMapper;

    public UserTelegramServiceImpl(UserTelegramRepository userTelegramRepository, UserTelegramMapper userTelegramMapper) {
        this.userTelegramRepository = userTelegramRepository;
        this.userTelegramMapper = userTelegramMapper;
    }

    /**
     * Save a userTelegram.
     *
     * @param userTelegramDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserTelegramDTO save(UserTelegramDTO userTelegramDTO) {
        log.debug("Request to save UserTelegram : {}", userTelegramDTO);
        UserTelegram userTelegram = userTelegramMapper.toEntity(userTelegramDTO);
        userTelegram = userTelegramRepository.save(userTelegram);
        return userTelegramMapper.toDto(userTelegram);
    }

    /**
     * Get all the userTelegrams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserTelegramDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserTelegrams");
        return userTelegramRepository.findAll(pageable)
            .map(userTelegramMapper::toDto);
    }

    /**
     * Get one userTelegram by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserTelegramDTO> findOne(Long id) {
        log.debug("Request to get UserTelegram : {}", id);
        return userTelegramRepository.findById(id)
            .map(userTelegramMapper::toDto);
    }

    /**
     * Delete the userTelegram by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserTelegram : {}", id);
        userTelegramRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserTelegramDTO> getOneByChatId(Long chatId) {
        log.debug("Request to get UserTelegram By ChatId : {}", chatId);
        return userTelegramRepository.findByChatId(chatId).map(userTelegramMapper::toDto);
    }

    @Override
    public Optional<UserTelegramDTO> getOneByStatus(Integer status) {
        log.debug("Request to get UserTelegram By ChatId : {}", status);
        return userTelegramRepository.findByStatus(status).map(userTelegramMapper::toDto);
    }
}

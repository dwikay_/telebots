package com.cakra.tech.bot.service.impl;

import com.cakra.tech.bot.service.OmsCommandService;
import com.cakra.tech.bot.domain.OmsCommand;
import com.cakra.tech.bot.repository.OmsCommandRepository;
import com.cakra.tech.bot.service.dto.OmsCommandDTO;
import com.cakra.tech.bot.service.mapper.OmsCommandMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link OmsCommand}.
 */
@Service
@Transactional
public class OmsCommandServiceImpl implements OmsCommandService {

    private final Logger log = LoggerFactory.getLogger(OmsCommandServiceImpl.class);

    private final OmsCommandRepository omsCommandRepository;

    private final OmsCommandMapper omsCommandMapper;

    public OmsCommandServiceImpl(OmsCommandRepository omsCommandRepository, OmsCommandMapper omsCommandMapper) {
        this.omsCommandRepository = omsCommandRepository;
        this.omsCommandMapper = omsCommandMapper;
    }

    /**
     * Save a omsCommand.
     *
     * @param omsCommandDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OmsCommandDTO save(OmsCommandDTO omsCommandDTO) {
        log.debug("Request to save OmsCommand : {}", omsCommandDTO);
        OmsCommand omsCommand = omsCommandMapper.toEntity(omsCommandDTO);
        omsCommand = omsCommandRepository.save(omsCommand);
        return omsCommandMapper.toDto(omsCommand);
    }

    /**
     * Get all the omsCommands.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<OmsCommandDTO> findAll() {
        log.debug("Request to get all OmsCommands");
        return omsCommandRepository.findAll().stream()
            .map(omsCommandMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one omsCommand by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OmsCommandDTO> findOne(Long id) {
        log.debug("Request to get OmsCommand : {}", id);
        return omsCommandRepository.findById(id)
            .map(omsCommandMapper::toDto);
    }

    /**
     * Delete the omsCommand by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OmsCommand : {}", id);
        omsCommandRepository.deleteById(id);
    }

    @Override
    public List<OmsCommandDTO> getAllByTipe(String tipe) {
        return omsCommandRepository.findAllByTipe(tipe)
            .stream()
            .map(omsCommandMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
}

package com.cakra.tech.bot.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.cakra.tech.bot.domain.UserTelegram} entity.
 */
public class UserTelegramDTO implements Serializable {

    private Long id;

    private String username;

    private String fullname;

    private Boolean isBot;

    private Long chatId;

    private Integer status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Boolean isIsBot() {
        return isBot;
    }

    public void setIsBot(Boolean isBot) {
        this.isBot = isBot;
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserTelegramDTO userTelegramDTO = (UserTelegramDTO) o;
        if (userTelegramDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userTelegramDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserTelegramDTO{" +
            "id=" + getId() +
            ", username='" + getUsername() + "'" +
            ", fullname='" + getFullname() + "'" +
            ", isBot='" + isIsBot() + "'" +
            ", chatId=" + getChatId() +
            ", status=" + getStatus() +
            "}";
    }
}

package com.cakra.tech.bot.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.cakra.tech.bot.domain.OmsCommand} entity.
 */
public class OmsCommandDTO implements Serializable {

    private Long id;

    private String command;

    private Integer callBackQuery;

    private String tipe;

    private String description;


    private Long clientId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Integer getCallBackQuery() {
        return callBackQuery;
    }

    public void setCallBackQuery(Integer callBackQuery) {
        this.callBackQuery = callBackQuery;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OmsCommandDTO omsCommandDTO = (OmsCommandDTO) o;
        if (omsCommandDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), omsCommandDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OmsCommandDTO{" +
            "id=" + getId() +
            ", command='" + getCommand() + "'" +
            ", callBackQuery=" + getCallBackQuery() +
            ", tipe='" + getTipe() + "'" +
            ", description='" + getDescription() + "'" +
            ", clientId=" + getClientId() +
            "}";
    }
}

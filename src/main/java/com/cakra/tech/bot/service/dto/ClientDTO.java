package com.cakra.tech.bot.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.cakra.tech.bot.domain.Client} entity.
 */
public class ClientDTO implements Serializable {

    private Long id;

    private String companyName;

    private String host;

    private Integer port;

    private String pathUrl;

    private String method;

    private String header;

    private Integer clientType;

    private String omsType;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getPathUrl() {
        return pathUrl;
    }

    public void setPathUrl(String pathUrl) {
        this.pathUrl = pathUrl;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Integer getClientType() {
        return clientType;
    }

    public void setClientType(Integer clientType) {
        this.clientType = clientType;
    }

    public String getOmsType() {
        return omsType;
    }

    public void setOmsType(String omsType) {
        this.omsType = omsType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientDTO clientDTO = (ClientDTO) o;
        if (clientDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientDTO{" +
            "id=" + getId() +
            ", companyName='" + getCompanyName() + "'" +
            ", host='" + getHost() + "'" +
            ", port=" + getPort() +
            ", pathUrl='" + getPathUrl() + "'" +
            ", method='" + getMethod() + "'" +
            ", header='" + getHeader() + "'" +
            ", clientType=" + getClientType() +
            ", omsType='" + getOmsType() + "'" +
            "}";
    }
}

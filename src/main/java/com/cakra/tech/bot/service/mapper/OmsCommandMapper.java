package com.cakra.tech.bot.service.mapper;


import com.cakra.tech.bot.domain.*;
import com.cakra.tech.bot.service.dto.OmsCommandDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OmsCommand} and its DTO {@link OmsCommandDTO}.
 */
@Mapper(componentModel = "spring", uses = {ClientMapper.class})
public interface OmsCommandMapper extends EntityMapper<OmsCommandDTO, OmsCommand> {

    @Mapping(source = "client.id", target = "clientId")
    OmsCommandDTO toDto(OmsCommand omsCommand);

    @Mapping(source = "clientId", target = "client")
    OmsCommand toEntity(OmsCommandDTO omsCommandDTO);

    default OmsCommand fromId(Long id) {
        if (id == null) {
            return null;
        }
        OmsCommand omsCommand = new OmsCommand();
        omsCommand.setId(id);
        return omsCommand;
    }
}

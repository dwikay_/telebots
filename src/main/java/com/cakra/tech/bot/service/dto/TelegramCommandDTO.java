package com.cakra.tech.bot.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.cakra.tech.bot.domain.TelegramCommand} entity.
 */
public class TelegramCommandDTO implements Serializable {

    private Long id;

    private String command;

    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TelegramCommandDTO telegramCommandDTO = (TelegramCommandDTO) o;
        if (telegramCommandDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), telegramCommandDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TelegramCommandDTO{" +
            "id=" + getId() +
            ", command='" + getCommand() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}

package com.cakra.tech.bot.service;

import com.cakra.tech.bot.service.dto.TelegramCommandDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.cakra.tech.bot.domain.TelegramCommand}.
 */
public interface TelegramCommandService {

    /**
     * Save a telegramCommand.
     *
     * @param telegramCommandDTO the entity to save.
     * @return the persisted entity.
     */
    TelegramCommandDTO save(TelegramCommandDTO telegramCommandDTO);

    /**
     * Get all the telegramCommands.
     *
     * @return the list of entities.
     */
    List<TelegramCommandDTO> findAll();

    /**
     * Get the "id" telegramCommand.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TelegramCommandDTO> findOne(Long id);

    /**
     * Delete the "id" telegramCommand.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);


    Optional<TelegramCommandDTO> getOneByCommand(String command);
}

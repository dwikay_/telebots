export interface IUserTelegram {
  id?: number;
  username?: string;
  fullname?: string;
  isBot?: boolean;
  chatId?: number;
  status?: number;
}

export class UserTelegram implements IUserTelegram {
  constructor(
    public id?: number,
    public username?: string,
    public fullname?: string,
    public isBot?: boolean,
    public chatId?: number,
    public status?: number
  ) {
    this.isBot = this.isBot || false;
  }
}

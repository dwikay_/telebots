export interface IClient {
  id?: number;
  companyName?: string;
  host?: string;
  port?: number;
  pathUrl?: string;
  method?: string;
  header?: string;
  clientType?: number;
  omsType?: string;
}

export class Client implements IClient {
  constructor(
    public id?: number,
    public companyName?: string,
    public host?: string,
    public port?: number,
    public pathUrl?: string,
    public method?: string,
    public header?: string,
    public clientType?: number,
    public omsType?: string
  ) {}
}

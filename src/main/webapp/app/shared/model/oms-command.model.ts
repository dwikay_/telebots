export interface IOmsCommand {
  id?: number;
  command?: string;
  callBackQuery?: number;
  tipe?: string;
  description?: string;
  clientId?: number;
}

export class OmsCommand implements IOmsCommand {
  constructor(
    public id?: number,
    public command?: string,
    public callBackQuery?: number,
    public tipe?: string,
    public description?: string,
    public clientId?: number
  ) {}
}

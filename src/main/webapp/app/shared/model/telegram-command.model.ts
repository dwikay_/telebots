export interface ITelegramCommand {
  id?: number;
  command?: string;
  description?: string;
}

export class TelegramCommand implements ITelegramCommand {
  constructor(public id?: number, public command?: string, public description?: string) {}
}

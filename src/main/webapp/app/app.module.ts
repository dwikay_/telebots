import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { CakrasupportBotSharedModule } from 'app/shared/shared.module';
import { CakrasupportBotCoreModule } from 'app/core/core.module';
import { CakrasupportBotAppRoutingModule } from './app-routing.module';
import { CakrasupportBotHomeModule } from './home/home.module';
import { CakrasupportBotEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    CakrasupportBotSharedModule,
    CakrasupportBotCoreModule,
    CakrasupportBotHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    CakrasupportBotEntityModule,
    CakrasupportBotAppRoutingModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent]
})
export class CakrasupportBotAppModule {}

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IUserTelegram, UserTelegram } from 'app/shared/model/user-telegram.model';
import { UserTelegramService } from './user-telegram.service';
import { UserTelegramComponent } from './user-telegram.component';
import { UserTelegramDetailComponent } from './user-telegram-detail.component';
import { UserTelegramUpdateComponent } from './user-telegram-update.component';

@Injectable({ providedIn: 'root' })
export class UserTelegramResolve implements Resolve<IUserTelegram> {
  constructor(private service: UserTelegramService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUserTelegram> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((userTelegram: HttpResponse<UserTelegram>) => {
          if (userTelegram.body) {
            return of(userTelegram.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new UserTelegram());
  }
}

export const userTelegramRoute: Routes = [
  {
    path: '',
    component: UserTelegramComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'UserTelegrams'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UserTelegramDetailComponent,
    resolve: {
      userTelegram: UserTelegramResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UserTelegrams'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UserTelegramUpdateComponent,
    resolve: {
      userTelegram: UserTelegramResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UserTelegrams'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UserTelegramUpdateComponent,
    resolve: {
      userTelegram: UserTelegramResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'UserTelegrams'
    },
    canActivate: [UserRouteAccessService]
  }
];

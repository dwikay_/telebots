import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CakrasupportBotSharedModule } from 'app/shared/shared.module';
import { UserTelegramComponent } from './user-telegram.component';
import { UserTelegramDetailComponent } from './user-telegram-detail.component';
import { UserTelegramUpdateComponent } from './user-telegram-update.component';
import { UserTelegramDeleteDialogComponent } from './user-telegram-delete-dialog.component';
import { userTelegramRoute } from './user-telegram.route';

@NgModule({
  imports: [CakrasupportBotSharedModule, RouterModule.forChild(userTelegramRoute)],
  declarations: [UserTelegramComponent, UserTelegramDetailComponent, UserTelegramUpdateComponent, UserTelegramDeleteDialogComponent],
  entryComponents: [UserTelegramDeleteDialogComponent]
})
export class CakrasupportBotUserTelegramModule {}

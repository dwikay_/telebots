import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IUserTelegram } from 'app/shared/model/user-telegram.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { UserTelegramService } from './user-telegram.service';
import { UserTelegramDeleteDialogComponent } from './user-telegram-delete-dialog.component';

@Component({
  selector: 'jhi-user-telegram',
  templateUrl: './user-telegram.component.html'
})
export class UserTelegramComponent implements OnInit, OnDestroy {
  userTelegrams?: IUserTelegram[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;

  constructor(
    protected userTelegramService: UserTelegramService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.userTelegramService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IUserTelegram[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInUserTelegrams();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IUserTelegram): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInUserTelegrams(): void {
    this.eventSubscriber = this.eventManager.subscribe('userTelegramListModification', () => this.loadPage());
  }

  delete(userTelegram: IUserTelegram): void {
    const modalRef = this.modalService.open(UserTelegramDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.userTelegram = userTelegram;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IUserTelegram[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/user-telegram'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.userTelegrams = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IUserTelegram, UserTelegram } from 'app/shared/model/user-telegram.model';
import { UserTelegramService } from './user-telegram.service';

@Component({
  selector: 'jhi-user-telegram-update',
  templateUrl: './user-telegram-update.component.html'
})
export class UserTelegramUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    username: [],
    fullname: [],
    isBot: [],
    chatId: [],
    status: []
  });

  constructor(protected userTelegramService: UserTelegramService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userTelegram }) => {
      this.updateForm(userTelegram);
    });
  }

  updateForm(userTelegram: IUserTelegram): void {
    this.editForm.patchValue({
      id: userTelegram.id,
      username: userTelegram.username,
      fullname: userTelegram.fullname,
      isBot: userTelegram.isBot,
      chatId: userTelegram.chatId,
      status: userTelegram.status
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const userTelegram = this.createFromForm();
    if (userTelegram.id !== undefined) {
      this.subscribeToSaveResponse(this.userTelegramService.update(userTelegram));
    } else {
      this.subscribeToSaveResponse(this.userTelegramService.create(userTelegram));
    }
  }

  private createFromForm(): IUserTelegram {
    return {
      ...new UserTelegram(),
      id: this.editForm.get(['id'])!.value,
      username: this.editForm.get(['username'])!.value,
      fullname: this.editForm.get(['fullname'])!.value,
      isBot: this.editForm.get(['isBot'])!.value,
      chatId: this.editForm.get(['chatId'])!.value,
      status: this.editForm.get(['status'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserTelegram>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserTelegram } from 'app/shared/model/user-telegram.model';

@Component({
  selector: 'jhi-user-telegram-detail',
  templateUrl: './user-telegram-detail.component.html'
})
export class UserTelegramDetailComponent implements OnInit {
  userTelegram: IUserTelegram | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ userTelegram }) => (this.userTelegram = userTelegram));
  }

  previousState(): void {
    window.history.back();
  }
}

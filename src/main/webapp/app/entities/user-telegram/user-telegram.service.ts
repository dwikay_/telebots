import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IUserTelegram } from 'app/shared/model/user-telegram.model';

type EntityResponseType = HttpResponse<IUserTelegram>;
type EntityArrayResponseType = HttpResponse<IUserTelegram[]>;

@Injectable({ providedIn: 'root' })
export class UserTelegramService {
  public resourceUrl = SERVER_API_URL + 'api/user-telegrams';

  constructor(protected http: HttpClient) {}

  create(userTelegram: IUserTelegram): Observable<EntityResponseType> {
    return this.http.post<IUserTelegram>(this.resourceUrl, userTelegram, { observe: 'response' });
  }

  update(userTelegram: IUserTelegram): Observable<EntityResponseType> {
    return this.http.put<IUserTelegram>(this.resourceUrl, userTelegram, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUserTelegram>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUserTelegram[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}

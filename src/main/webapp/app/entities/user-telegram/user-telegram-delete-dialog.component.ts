import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserTelegram } from 'app/shared/model/user-telegram.model';
import { UserTelegramService } from './user-telegram.service';

@Component({
  templateUrl: './user-telegram-delete-dialog.component.html'
})
export class UserTelegramDeleteDialogComponent {
  userTelegram?: IUserTelegram;

  constructor(
    protected userTelegramService: UserTelegramService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.userTelegramService.delete(id).subscribe(() => {
      this.eventManager.broadcast('userTelegramListModification');
      this.activeModal.close();
    });
  }
}

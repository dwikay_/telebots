import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITelegramCommand } from 'app/shared/model/telegram-command.model';
import { TelegramCommandService } from './telegram-command.service';
import { TelegramCommandDeleteDialogComponent } from './telegram-command-delete-dialog.component';

@Component({
  selector: 'jhi-telegram-command',
  templateUrl: './telegram-command.component.html'
})
export class TelegramCommandComponent implements OnInit, OnDestroy {
  telegramCommands?: ITelegramCommand[];
  eventSubscriber?: Subscription;

  constructor(
    protected telegramCommandService: TelegramCommandService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.telegramCommandService.query().subscribe((res: HttpResponse<ITelegramCommand[]>) => (this.telegramCommands = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInTelegramCommands();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITelegramCommand): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTelegramCommands(): void {
    this.eventSubscriber = this.eventManager.subscribe('telegramCommandListModification', () => this.loadAll());
  }

  delete(telegramCommand: ITelegramCommand): void {
    const modalRef = this.modalService.open(TelegramCommandDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.telegramCommand = telegramCommand;
  }
}

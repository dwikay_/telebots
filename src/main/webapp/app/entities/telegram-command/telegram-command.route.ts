import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITelegramCommand, TelegramCommand } from 'app/shared/model/telegram-command.model';
import { TelegramCommandService } from './telegram-command.service';
import { TelegramCommandComponent } from './telegram-command.component';
import { TelegramCommandDetailComponent } from './telegram-command-detail.component';
import { TelegramCommandUpdateComponent } from './telegram-command-update.component';

@Injectable({ providedIn: 'root' })
export class TelegramCommandResolve implements Resolve<ITelegramCommand> {
  constructor(private service: TelegramCommandService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITelegramCommand> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((telegramCommand: HttpResponse<TelegramCommand>) => {
          if (telegramCommand.body) {
            return of(telegramCommand.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TelegramCommand());
  }
}

export const telegramCommandRoute: Routes = [
  {
    path: '',
    component: TelegramCommandComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TelegramCommands'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TelegramCommandDetailComponent,
    resolve: {
      telegramCommand: TelegramCommandResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TelegramCommands'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TelegramCommandUpdateComponent,
    resolve: {
      telegramCommand: TelegramCommandResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TelegramCommands'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TelegramCommandUpdateComponent,
    resolve: {
      telegramCommand: TelegramCommandResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'TelegramCommands'
    },
    canActivate: [UserRouteAccessService]
  }
];

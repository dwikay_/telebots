import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITelegramCommand } from 'app/shared/model/telegram-command.model';

type EntityResponseType = HttpResponse<ITelegramCommand>;
type EntityArrayResponseType = HttpResponse<ITelegramCommand[]>;

@Injectable({ providedIn: 'root' })
export class TelegramCommandService {
  public resourceUrl = SERVER_API_URL + 'api/telegram-commands';

  constructor(protected http: HttpClient) {}

  create(telegramCommand: ITelegramCommand): Observable<EntityResponseType> {
    return this.http.post<ITelegramCommand>(this.resourceUrl, telegramCommand, { observe: 'response' });
  }

  update(telegramCommand: ITelegramCommand): Observable<EntityResponseType> {
    return this.http.put<ITelegramCommand>(this.resourceUrl, telegramCommand, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITelegramCommand>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITelegramCommand[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}

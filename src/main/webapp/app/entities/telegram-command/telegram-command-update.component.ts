import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ITelegramCommand, TelegramCommand } from 'app/shared/model/telegram-command.model';
import { TelegramCommandService } from './telegram-command.service';

@Component({
  selector: 'jhi-telegram-command-update',
  templateUrl: './telegram-command-update.component.html'
})
export class TelegramCommandUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    command: [],
    description: []
  });

  constructor(
    protected telegramCommandService: TelegramCommandService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ telegramCommand }) => {
      this.updateForm(telegramCommand);
    });
  }

  updateForm(telegramCommand: ITelegramCommand): void {
    this.editForm.patchValue({
      id: telegramCommand.id,
      command: telegramCommand.command,
      description: telegramCommand.description
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const telegramCommand = this.createFromForm();
    if (telegramCommand.id !== undefined) {
      this.subscribeToSaveResponse(this.telegramCommandService.update(telegramCommand));
    } else {
      this.subscribeToSaveResponse(this.telegramCommandService.create(telegramCommand));
    }
  }

  private createFromForm(): ITelegramCommand {
    return {
      ...new TelegramCommand(),
      id: this.editForm.get(['id'])!.value,
      command: this.editForm.get(['command'])!.value,
      description: this.editForm.get(['description'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITelegramCommand>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}

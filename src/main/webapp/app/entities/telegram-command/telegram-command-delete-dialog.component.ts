import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITelegramCommand } from 'app/shared/model/telegram-command.model';
import { TelegramCommandService } from './telegram-command.service';

@Component({
  templateUrl: './telegram-command-delete-dialog.component.html'
})
export class TelegramCommandDeleteDialogComponent {
  telegramCommand?: ITelegramCommand;

  constructor(
    protected telegramCommandService: TelegramCommandService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.telegramCommandService.delete(id).subscribe(() => {
      this.eventManager.broadcast('telegramCommandListModification');
      this.activeModal.close();
    });
  }
}

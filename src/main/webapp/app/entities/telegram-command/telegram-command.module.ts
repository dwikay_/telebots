import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CakrasupportBotSharedModule } from 'app/shared/shared.module';
import { TelegramCommandComponent } from './telegram-command.component';
import { TelegramCommandDetailComponent } from './telegram-command-detail.component';
import { TelegramCommandUpdateComponent } from './telegram-command-update.component';
import { TelegramCommandDeleteDialogComponent } from './telegram-command-delete-dialog.component';
import { telegramCommandRoute } from './telegram-command.route';

@NgModule({
  imports: [CakrasupportBotSharedModule, RouterModule.forChild(telegramCommandRoute)],
  declarations: [
    TelegramCommandComponent,
    TelegramCommandDetailComponent,
    TelegramCommandUpdateComponent,
    TelegramCommandDeleteDialogComponent
  ],
  entryComponents: [TelegramCommandDeleteDialogComponent]
})
export class CakrasupportBotTelegramCommandModule {}

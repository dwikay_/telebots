import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITelegramCommand } from 'app/shared/model/telegram-command.model';

@Component({
  selector: 'jhi-telegram-command-detail',
  templateUrl: './telegram-command-detail.component.html'
})
export class TelegramCommandDetailComponent implements OnInit {
  telegramCommand: ITelegramCommand | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ telegramCommand }) => (this.telegramCommand = telegramCommand));
  }

  previousState(): void {
    window.history.back();
  }
}

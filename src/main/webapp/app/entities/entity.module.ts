import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.CakrasupportBotClientModule)
      },
      {
        path: 'user-telegram',
        loadChildren: () => import('./user-telegram/user-telegram.module').then(m => m.CakrasupportBotUserTelegramModule)
      },
      {
        path: 'oms-command',
        loadChildren: () => import('./oms-command/oms-command.module').then(m => m.CakrasupportBotOmsCommandModule)
      },
      {
        path: 'telegram-command',
        loadChildren: () => import('./telegram-command/telegram-command.module').then(m => m.CakrasupportBotTelegramCommandModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class CakrasupportBotEntityModule {}

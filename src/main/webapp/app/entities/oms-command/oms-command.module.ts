import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CakrasupportBotSharedModule } from 'app/shared/shared.module';
import { OmsCommandComponent } from './oms-command.component';
import { OmsCommandDetailComponent } from './oms-command-detail.component';
import { OmsCommandUpdateComponent } from './oms-command-update.component';
import { OmsCommandDeleteDialogComponent } from './oms-command-delete-dialog.component';
import { omsCommandRoute } from './oms-command.route';

@NgModule({
  imports: [CakrasupportBotSharedModule, RouterModule.forChild(omsCommandRoute)],
  declarations: [OmsCommandComponent, OmsCommandDetailComponent, OmsCommandUpdateComponent, OmsCommandDeleteDialogComponent],
  entryComponents: [OmsCommandDeleteDialogComponent]
})
export class CakrasupportBotOmsCommandModule {}

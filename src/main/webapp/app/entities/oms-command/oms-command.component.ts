import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IOmsCommand } from 'app/shared/model/oms-command.model';
import { OmsCommandService } from './oms-command.service';
import { OmsCommandDeleteDialogComponent } from './oms-command-delete-dialog.component';

@Component({
  selector: 'jhi-oms-command',
  templateUrl: './oms-command.component.html'
})
export class OmsCommandComponent implements OnInit, OnDestroy {
  omsCommands?: IOmsCommand[];
  eventSubscriber?: Subscription;

  constructor(protected omsCommandService: OmsCommandService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.omsCommandService.query().subscribe((res: HttpResponse<IOmsCommand[]>) => (this.omsCommands = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInOmsCommands();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IOmsCommand): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInOmsCommands(): void {
    this.eventSubscriber = this.eventManager.subscribe('omsCommandListModification', () => this.loadAll());
  }

  delete(omsCommand: IOmsCommand): void {
    const modalRef = this.modalService.open(OmsCommandDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.omsCommand = omsCommand;
  }
}

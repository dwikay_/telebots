import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOmsCommand } from 'app/shared/model/oms-command.model';

type EntityResponseType = HttpResponse<IOmsCommand>;
type EntityArrayResponseType = HttpResponse<IOmsCommand[]>;

@Injectable({ providedIn: 'root' })
export class OmsCommandService {
  public resourceUrl = SERVER_API_URL + 'api/oms-commands';

  constructor(protected http: HttpClient) {}

  create(omsCommand: IOmsCommand): Observable<EntityResponseType> {
    return this.http.post<IOmsCommand>(this.resourceUrl, omsCommand, { observe: 'response' });
  }

  update(omsCommand: IOmsCommand): Observable<EntityResponseType> {
    return this.http.put<IOmsCommand>(this.resourceUrl, omsCommand, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IOmsCommand>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IOmsCommand[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}

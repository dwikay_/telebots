import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IOmsCommand, OmsCommand } from 'app/shared/model/oms-command.model';
import { OmsCommandService } from './oms-command.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client/client.service';

@Component({
  selector: 'jhi-oms-command-update',
  templateUrl: './oms-command-update.component.html'
})
export class OmsCommandUpdateComponent implements OnInit {
  isSaving = false;
  clients: IClient[] = [];

  editForm = this.fb.group({
    id: [],
    command: [],
    callBackQuery: [],
    tipe: [],
    description: [],
    clientId: []
  });

  constructor(
    protected omsCommandService: OmsCommandService,
    protected clientService: ClientService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ omsCommand }) => {
      this.updateForm(omsCommand);

      this.clientService.query().subscribe((res: HttpResponse<IClient[]>) => (this.clients = res.body || []));
    });
  }

  updateForm(omsCommand: IOmsCommand): void {
    this.editForm.patchValue({
      id: omsCommand.id,
      command: omsCommand.command,
      callBackQuery: omsCommand.callBackQuery,
      tipe: omsCommand.tipe,
      description: omsCommand.description,
      clientId: omsCommand.clientId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const omsCommand = this.createFromForm();
    if (omsCommand.id !== undefined) {
      this.subscribeToSaveResponse(this.omsCommandService.update(omsCommand));
    } else {
      this.subscribeToSaveResponse(this.omsCommandService.create(omsCommand));
    }
  }

  private createFromForm(): IOmsCommand {
    return {
      ...new OmsCommand(),
      id: this.editForm.get(['id'])!.value,
      command: this.editForm.get(['command'])!.value,
      callBackQuery: this.editForm.get(['callBackQuery'])!.value,
      tipe: this.editForm.get(['tipe'])!.value,
      description: this.editForm.get(['description'])!.value,
      clientId: this.editForm.get(['clientId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOmsCommand>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IClient): any {
    return item.id;
  }
}

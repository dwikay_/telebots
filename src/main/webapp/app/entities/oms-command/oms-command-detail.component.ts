import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOmsCommand } from 'app/shared/model/oms-command.model';

@Component({
  selector: 'jhi-oms-command-detail',
  templateUrl: './oms-command-detail.component.html'
})
export class OmsCommandDetailComponent implements OnInit {
  omsCommand: IOmsCommand | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ omsCommand }) => (this.omsCommand = omsCommand));
  }

  previousState(): void {
    window.history.back();
  }
}

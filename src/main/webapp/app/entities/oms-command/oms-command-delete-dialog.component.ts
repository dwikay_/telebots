import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOmsCommand } from 'app/shared/model/oms-command.model';
import { OmsCommandService } from './oms-command.service';

@Component({
  templateUrl: './oms-command-delete-dialog.component.html'
})
export class OmsCommandDeleteDialogComponent {
  omsCommand?: IOmsCommand;

  constructor(
    protected omsCommandService: OmsCommandService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.omsCommandService.delete(id).subscribe(() => {
      this.eventManager.broadcast('omsCommandListModification');
      this.activeModal.close();
    });
  }
}

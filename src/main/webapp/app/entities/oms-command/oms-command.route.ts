import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IOmsCommand, OmsCommand } from 'app/shared/model/oms-command.model';
import { OmsCommandService } from './oms-command.service';
import { OmsCommandComponent } from './oms-command.component';
import { OmsCommandDetailComponent } from './oms-command-detail.component';
import { OmsCommandUpdateComponent } from './oms-command-update.component';

@Injectable({ providedIn: 'root' })
export class OmsCommandResolve implements Resolve<IOmsCommand> {
  constructor(private service: OmsCommandService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IOmsCommand> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((omsCommand: HttpResponse<OmsCommand>) => {
          if (omsCommand.body) {
            return of(omsCommand.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new OmsCommand());
  }
}

export const omsCommandRoute: Routes = [
  {
    path: '',
    component: OmsCommandComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OmsCommands'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OmsCommandDetailComponent,
    resolve: {
      omsCommand: OmsCommandResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OmsCommands'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OmsCommandUpdateComponent,
    resolve: {
      omsCommand: OmsCommandResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OmsCommands'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OmsCommandUpdateComponent,
    resolve: {
      omsCommand: OmsCommandResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'OmsCommands'
    },
    canActivate: [UserRouteAccessService]
  }
];
